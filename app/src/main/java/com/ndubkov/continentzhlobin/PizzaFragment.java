package com.ndubkov.continentzhlobin;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Pizza list fragment
 */
public class PizzaFragment extends Fragment {

    RecyclerView mRecyclerView;
    View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_pizza, container, false);
        return mRootView;
    }

}
